import * as React from 'react';
import Box from '@mui/material/Box';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import { Grid } from "@mui/material";


export default function SelectNumberPage(props) {
    const [show, setShow] = React.useState('');
    var vShow = 10;
    const handleChange = (event) => {
        setShow(event.target.value);
        vShow = event.target.value
        console.log(vShow)
        props.parentCallback(vShow);
    };



    return (
        <Box >
            <Grid container marginBottom={5}>
                <FormControl variant="standard" sx={{ m: 1, minWidth: 120 }}>
                    <InputLabel id="demo-simple-select-label">Show</InputLabel>
                    <Select
                        labelId="demo-simple-select-label"
                        id="demo-simple-select"
                        value={show}
                        label="show"
                        onChange={handleChange}
                    >
                        <MenuItem value={5}>5</MenuItem>
                        <MenuItem value={10}>10</MenuItem>
                        <MenuItem value={25}>25</MenuItem>
                        <MenuItem value={50}>50</MenuItem>

                    </Select>
                </FormControl>
            </Grid>

        </Box>
    );
}
