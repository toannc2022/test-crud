import { Modal, Box, Grid, TextField, Select, FormControl, MenuItem, InputLabel, Button, Stack } from "@mui/material";
import { useState, useEffect } from 'react';
import * as React from 'react';
import { styled } from '@mui/material/styles';

import Paper from '@mui/material/Paper';
import ModalToast from './ModalToast.js';


const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 1000,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
};

const Item = styled(Paper)(({ theme }) => ({
    backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
    ...theme.typography.body2,
    padding: theme.spacing(1),
    textAlign: 'center',
    color: theme.palette.text.secondary,
}));


function InsertModal({ open, setOpen }) {
    const fetchApi = async (paramUrl, paramOptions = {}) => {
        const response = await fetch(paramUrl, paramOptions);
        const responseData = await response.json();
        return responseData;
    }
    const [openModalToast, setOpenModalToast] = useState(false);

    const [openXacNhan, setOpenXacNhan] = useState(false);
    const [infoToast, setInfoToast] = useState({});

    const [dataDrink, setDataDrink] = useState([]);
    const [sizeCombo, setSizeCombo] = useState("");
    const [TypePizza, setTypePizza] = useState("");
    const [drink, setDrink] = useState('');

    const [hoTen, setHoTen] = useState('')
    const [soDienThoai, setSoDienThoai] = useState('')
    const [email, setEmail] = useState('')
    const [diaChi, setDiaChi] = useState('')
    const [voucherID, setVoucherID] = useState('');
    const [loiNhan, setLoiNhan] = useState('')
    const [phanTramGiamGia, setPhanTramGiamGia] = useState("");

    const getDataDrink = async () => {
        const response = await fetch("http://42.115.221.44:8080/devcamp-pizza365/drinks");
        const data = await response.json();
        return data;
    }

    const getDataVoucher = async () => {
        const response = await fetch("http://42.115.221.44:8080/devcamp-voucher-api/voucher_detail/" + voucherID);
        const data = await response.json();
        return data;
    }

    const insertClick = (e) => {
        if (validate()) {
            getDataVoucher()
                .then((data) => {
                    console.log(data);
                    setPhanTramGiamGia(data.phanTramGiamGia);
                })
                .catch((error) => {
                    console.log(error);
                    setPhanTramGiamGia("0");
                })
            setOpenXacNhan(true);
        }

    }

    const xacNhanClick = () => {
        if (validate()) {
            const body = {
                method: 'POST',
                body: JSON.stringify({
                    kichCo: sizeCombo.kichCo,
                    duongKinh: sizeCombo.duongKinh,
                    suon: sizeCombo.suon,
                    salad: sizeCombo.salad,
                    loaiPizza: TypePizza,
                    idVourcher: voucherID,
                    idLoaiNuocUong: drink,
                    soLuongNuoc: sizeCombo.soLuongNuoc,
                    hoTen: hoTen,
                    thanhTien: sizeCombo.thanhTien,
                    email: email,
                    soDienThoai: soDienThoai,
                    diaChi: diaChi,
                    loiNhan: loiNhan
                }),
                headers: {
                    'Content-type': 'application/json; charset=UTF-8',
                },
            }
            fetchApi('http://42.115.221.44:8080/devcamp-pizza365/orders', body)
                .then((data) => {
                    console.log(data);
                    setOpenModalToast(true)
                    setInfoToast({ toastColor: "green", toastThongBao: "Chúc mừng đã thêm order thành công!", toastNoiDung: "Mã đơn hàng : " + data.orderId })


                })
                .catch((error) => {
                    console.log(error);

                })
            setOpen(false);
            setOpenXacNhan(false);
        }
    }
    const comboChange = (e) => {
        if (e.target.value === "S") {
            setSizeCombo({
                kichCo: "S",
                duongKinh: "20cm",
                suon: "2",
                salad: "200g",
                soLuongNuoc: "2",
                thanhTien: "150000"
            });
        }
        if (e.target.value === "M") {
            setSizeCombo({
                kichCo: "M",
                duongKinh: "25cm",
                suon: "4",
                salad: "300g",
                soLuongNuoc: "3",
                thanhTien: "200000"
            });
        }
        if (e.target.value === "L") {
            setSizeCombo({
                kichCo: "L",
                duongKinh: "30cm",
                suon: "8",
                salad: "500g",
                soLuongNuoc: "4",
                thanhTien: "250000"
            });
        }
    }
    const typeChange = (e) => {
        setTypePizza(e.target.value)
    }

    const drinkChange = (e) => {
        setDrink(e.target.value)
    }
    const hoTenChange = (e) => {
        setHoTen(e.target.value)
    }
    const soDienThoaiChange = (e) => {
        setSoDienThoai(e.target.value)
    }
    const emailChange = (e) => {
        setEmail(e.target.value)
    }
    const diaChiChange = (e) => {
        setDiaChi(e.target.value)
    }
    const voucherChange = (e) => {
        setVoucherID(e.target.value)

    }
    const loiNhanChange = (e) => {
        setLoiNhan(e.target.value)
    }


    const handleClose = () => setOpen(false);
    const handleChange = () => setOpen(false);
    const handleCloseXacNhan = () => setOpenXacNhan(false);

    const validate = () => {
        var mailformat = /^\w+([-]?\w+)*@\w+([-]?\w+)*(\.\w{2,3})+$/;
        if (sizeCombo === "") {
            console.log("chưa nhập sizeCombo")
            setOpenModalToast(true)
            setInfoToast({ toastColor: "--pink-color", toastThongBao: "Vui lòng điền đầy đủ thông tin!", toastNoiDung: "chưa nhập Size Combo" })
            return false;
        }
        if (TypePizza === "") {
            console.log("chưa nhập typePizza")
            setOpenModalToast(true)
            setInfoToast({ toastColor: "--pink-color", toastThongBao: "Vui lòng điền đầy đủ thông tin!", toastNoiDung: "chưa nhập Type Pizza" })
            return false;
        }
        if (drink === "") {
            console.log("chưa nhập drink")
            setOpenModalToast(true)
            setInfoToast({ toastColor: "--pink-color", toastThongBao: "Vui lòng điền đầy đủ thông tin!", toastNoiDung: "chưa nhập nước uống" })
            return false;
        }
        if (hoTen === "") {
            console.log("chưa nhập họ tên")
            setOpenModalToast(true)
            setInfoToast({ toastColor: "--pink-color", toastThongBao: "Vui lòng điền đầy đủ thông tin!", toastNoiDung: "chưa nhập họ tên" })
            return false;
        }
        if (soDienThoai === "") {
            console.log("chưa nhập sodienthoai")
            setOpenModalToast(true)
            setInfoToast({ toastColor: "--pink-color", toastThongBao: "Vui lòng điền đầy đủ thông tin!", toastNoiDung: "chưa nhập số điện thoại" })
            return false;
        }
        if (isNaN(soDienThoai) || soDienThoai.length !== 10) {
            console.log("Số điện thoại phải là số có 10 chữ số!")
            setOpenModalToast(true)
            setInfoToast({ toastColor: "--pink-color", toastThongBao: "Vui lòng điền đầy đủ thông tin!", toastNoiDung: "Số điện thoại phải là số có 10 chữ số!" })
            return false;
        }
        if (email !== "" && !email.match(mailformat)) {
            console.log("Email nhập vào không hợp lệ!")
            setOpenModalToast(true)
            setInfoToast({ toastColor: "--pink-color", toastThongBao: "Vui lòng điền đầy đủ thông tin!", toastNoiDung: "Email nhập vào không hợp lệ!" })
            return false;
        }

        if (diaChi === "") {
            console.log("chưa nhập diachi")
            setOpenModalToast(true)
            setInfoToast({ toastColor: "--pink-color", toastThongBao: "Vui lòng điền đầy đủ thông tin!", toastNoiDung: "chưa nhập địa chỉ" })
            return false;
        }
        if (voucherID === "") {
            console.log("chưa nhập magiamgia")
        }
        if (loiNhan === "") {
            console.log("chưa nhập loinhan")
        }


        return true;
    }

    useEffect(() => {
        getDataDrink()
            .then((data) => {
                console.log(data);
                setDataDrink(data);
            })
            .catch((error) => {
                console.log(error);
            })

    }, [])

    return (
        <>
            <Modal
                open={open}
                onClose={handleClose}
                aria-labelledby="modal-detail-title"
                aria-describedby="modal-detail-description"
            >
                <Box sx={style}>
                    <h2>Thêm Order</h2>
                    <hr />
                    <Grid mt={5}>
                        <Box sx={{ minWidth: 120 }}>


                            <Grid container spacing={2}>
                                <Grid item xs={4}>
                                    <Item><FormControl fullWidth>
                                        <InputLabel id="demo-simple-select-label">Kích cỡ Combo</InputLabel>
                                        <Select
                                            label="Kích cỡ Combo"
                                            onChange={comboChange}
                                        >
                                            <MenuItem value="S">S</MenuItem>
                                            <MenuItem value='M'>M</MenuItem>
                                            <MenuItem value='L'>L</MenuItem>
                                        </Select>
                                    </FormControl></Item>
                                </Grid>
                                <Grid item xs={4}>
                                    <Item><TextField variant="outlined" label="Đường kính Pizza" fullWidth value={sizeCombo.duongKinh} defaultValue=" " disabled></TextField></Item>
                                </Grid>
                                <Grid item xs={4}>
                                    <Item><TextField variant="outlined" label="Salad" fullWidth value={sizeCombo.salad} defaultValue=" " disabled></TextField></Item>
                                </Grid>
                                <Grid item xs={4}>
                                    <Item><TextField variant="outlined" label="Số lượng nước" fullWidth value={sizeCombo.soLuongNuoc} defaultValue=" " disabled></TextField></Item>
                                </Grid>
                                <Grid item xs={4}>
                                    <Item><TextField variant="outlined" label="Sườn" fullWidth value={sizeCombo.suon} defaultValue=" " disabled></TextField></Item>
                                </Grid>
                                <Grid item xs={4}>
                                    <Item><TextField variant="outlined" label="Thành Tiền" fullWidth value={sizeCombo.thanhTien} defaultValue=" " disabled></TextField></Item>
                                </Grid>
                            </Grid>
                        </Box>
                    </Grid>

                    <Grid mt={5} mb={5}>
                        <Box sx={{ minWidth: 120 }}>

                            <Grid container spacing={2}>
                                <Grid item xs={6}>
                                    <FormControl fullWidth>
                                        <InputLabel >Loại Pizza</InputLabel>
                                        <Select
                                            label="Loại Pizza"
                                            onChange={typeChange}
                                        >
                                            <MenuItem value="OCEAN MANIA">OCEAN MANIA</MenuItem>
                                            <MenuItem value='HAWAIIAN'>HAWAIIAN</MenuItem>
                                            <MenuItem value='CHEESY CHICKEN BACON'>CHEESY CHICKEN BACON</MenuItem>
                                        </Select>
                                    </FormControl>
                                </Grid>
                                <Grid item xs={6}>
                                    <FormControl fullWidth>
                                        <InputLabel >Loại Nước Uống</InputLabel>
                                        <Select
                                            label="Loại Nước Uống"
                                            onChange={drinkChange}
                                        >
                                            {dataDrink.map((drink, index) => (
                                                <MenuItem key={index} value={drink.maNuocUong}>{drink.tenNuocUong}</MenuItem>
                                            ))}

                                        </Select>
                                    </FormControl>
                                </Grid>
                            </Grid>
                        </Box>
                    </Grid>

                    <Grid mt={5} mb={5}>
                        <Box sx={{ minWidth: 120 }}>

                            <Grid container spacing={2}>

                                <Grid item xs={6}>
                                    <TextField variant="outlined" label="Họ và Tên" fullWidth onChange={hoTenChange}></TextField>
                                </Grid>
                                <Grid item xs={6}>
                                    <TextField variant="outlined" label="Số điện thoại" fullWidth onChange={soDienThoaiChange}></TextField>
                                </Grid>
                                <Grid item xs={6}>
                                    <TextField variant="outlined" label="Email" fullWidth onChange={emailChange}></TextField>
                                </Grid>
                                <Grid item xs={6}>
                                    <TextField variant="outlined" label="Địa chỉ" fullWidth onChange={diaChiChange}></TextField>
                                </Grid>
                                <Grid item xs={6}>
                                    <TextField variant="outlined" label="Voucher" fullWidth onChange={voucherChange}></TextField>
                                </Grid>
                                <Grid item xs={6}>
                                    <TextField variant="outlined" label="Lời nhắn" fullWidth onChange={loiNhanChange}></TextField>
                                </Grid>
                            </Grid>
                        </Box>
                    </Grid>
                    <hr />
                    <Box sx={{ mt: 5, mx: 45 }} >
                        <Stack spacing={2} direction="row" align='center'>
                            <Button color='success' variant="contained" size="medium" onClick={() => insertClick()}>Insert</Button>
                            <Button color='success' variant="outlined" size="medium" onClick={handleChange}>Cancel</Button>
                        </Stack>
                    </Box>
                </Box>


            </Modal>
            <Modal
                open={openXacNhan}
                onClose={handleCloseXacNhan}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box >
                    <Box>
                        <Grid>
                            <Grid id="modal__inner">
                                <Grid id="modal__header" style={{ backgroundColor: "green" }}>
                                    <h4>Thông tin đơn hàng:</h4>
                                </Grid>
                                <Grid id="modal__body" >

                                    {"SizeCombo : " + sizeCombo.kichCo} <br />
                                    {"TypePizza : " + TypePizza} <br />
                                    {"Nước Uống : " + drink} <br />
                                    {"Tên : " + hoTen} <br />
                                    {"Email : " + email} <br />
                                    {"Số Điện Thoại : " + soDienThoai} <br />
                                    {"Địa Chỉ : " + diaChi}<br />
                                    {"Mã Giảm Giá : " + voucherID}<br />
                                    {" Phần Trăm Giảm Giá : " + phanTramGiamGia + "%"}  <br />
                                    {"Lời nhắn : " + loiNhan} <br />
                                    <hr />
                                </Grid>
                                <Box sx={{ mx: 24, mb: 5 }} >
                                    <Stack spacing={2} direction="row" align='center'>
                                        <Button color='success' variant="contained" size="medium" onClick={xacNhanClick}>Xác nhận</Button>
                                        <Button color='success' variant="outlined" size="medium" onClick={handleCloseXacNhan}>Cancel</Button>
                                    </Stack>
                                </Box>
                            </Grid>
                        </Grid>

                    </Box>


                </Box>
            </Modal>
            <ModalToast open={openModalToast} setOpen={setOpenModalToast} thongBao={infoToast} />
        </>
    )
}

export default InsertModal;
